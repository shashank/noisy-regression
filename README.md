# noisy-regression
This repo contains a Python implementation of simulations supporting a research project on learning regression models in the presence of label noise.

This README will be updated as the project approaches publication.

## Author
[Shashank Singh](https://sss1.github.io/), Post-Doctoral Researcher at the Max Planck Institute for Intelligent Systems

## License
[CC BY 4.0 Attribution International](https://creativecommons.org/licenses/by/4.0/)
